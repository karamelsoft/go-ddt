package gotest

import (
	binaryfile "gitlab.com/karamelsoft/gotest/file/binary"
	"gitlab.com/karamelsoft/gotest/file/text"
	"strings"
	"testing"
)

func TestSingleFromValue(t *testing.T) {
	NewTest(t, "single_value").
		Single(func(scenario EmptyTestScenario) {
			scenario.Value("hello world!").
				Map(func(text interface{}) interface{} {
					return strings.ToUpper(text.(string))
				}).
				Map(func(text interface{}) interface{} {
					return []byte(text.(string))
				}).
				Save(binaryfile.NewBinarySaver(), "uppercase").
				Compare("uppercase", binaryfile.NewBinaryComparator())
		})
}

func TestSingleFromLoad(t *testing.T) {
	NewTest(t, "single_file").
		Single(func(scenario EmptyTestScenario) {
			scenario.Load("message", binaryfile.NewBinaryLoader()).
				Map(func(value interface{}) interface{} {
					return string(value.([]byte))
				}).
				Map(func(text interface{}) interface{} {
					return strings.ToUpper(text.(string))
				}).
				Map(func(text interface{}) interface{} {
					return []byte(text.(string))
				}).
				Save(binaryfile.NewBinarySaver(), "uppercase").
				Compare("uppercase", binaryfile.NewBinaryComparator())
		})
}

func TestForEach(t *testing.T) {
	NewTest(t, "foreach").
		ForEach(func(scenario EmptyTestScenario) {
			scenario.Load("person.txt.txt", text.NewTextLoader()).
				Map(func(value interface{}) interface{} {
					return strings.ToUpper(value.(string))
				}).
				Save(text.NewTextSaver(), "person.txt.txt").
				Compare("person.txt.txt", text.NewTextComparator())
		})
}
