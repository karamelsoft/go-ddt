package gotest

import (
	"fmt"
	"gitlab.com/karamelsoft/gotest/file"
	"io"
	"os"
	"path/filepath"
	"testing"
)

const (
	InputDirectory    = "input"
	ActualDirectory   = "actual"
	ExpectedDirectory = "expected"
	ErrorDirectory    = "error"
)

type EmptyTestScenario interface {
	Value(value interface{}) ReadyTestScenario
	ValueFrom(supplier ValueSupplier) ReadyTestScenario
	ValueFromUnsafe(supplier UnsafeValueSupplier) ReadyTestScenario
	Load(fileName string, loader file.Loader) ReadyTestScenario
	Script(script Script) ReadyTestScenario
}

type ReadyTestScenario interface {
	EmptyTestScenario
	Map(mapping Function) ReadyTestScenario
	MapUnsafe(mapping UnsafeFunction) ReadyTestScenario
	Consume(consumer Consumer) ReadyTestScenario
	ConsumeUnsafe(consumer UnsafeConsumer) ReadyTestScenario
	Save(saver file.Saver, fileName string) ReadyTestScenario
	Compare(fileName string, comparator file.Comparator) ReadyTestScenario
	ContinueWith(script ContinuationScript) ReadyTestScenario
}

type Scenario func(scenario EmptyTestScenario)
type Script func(scenario EmptyTestScenario) (ReadyTestScenario, error)
type ContinuationScript func(scenario ReadyTestScenario) (ReadyTestScenario, error)
type ScenarioSupplier func() (ReadyTestScenario, error)
type ValueSupplier func() interface{}
type Function func(value interface{}) interface{}
type Consumer func(value interface{})
type UnsafeValueSupplier func() (interface{}, error)
type UnsafeFunction func(value interface{}) (interface{}, error)
type UnsafeConsumer func(value interface{}) error

type TestScenario struct {
	t                 *testing.T
	path              string
	inputDirectory    string
	actualDirectory   string
	expectedDirectory string
	errorDirectory    string
	value             interface{}
}

func NewScenario(t *testing.T, path string) *TestScenario {
	return &TestScenario{
		t:                 t,
		path:              path,
		inputDirectory:    filepath.Join(path, InputDirectory),
		actualDirectory:   filepath.Join(path, ActualDirectory),
		expectedDirectory: filepath.Join(path, ExpectedDirectory),
		errorDirectory:    filepath.Join(path, ErrorDirectory),
	}
}

func (ts *TestScenario) Execute(scenario Scenario) {
	_ = os.RemoveAll(ts.actualDirectory)
	_ = os.RemoveAll(ts.errorDirectory)
	scenario(ts)
}

func (ts *TestScenario) Script(script Script) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		return script(ts)
	})
}

func (ts *TestScenario) ContinueWith(script ContinuationScript) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		return script(ts)
	})
}

func (ts *TestScenario) Value(value interface{}) ReadyTestScenario {
	ts.value = value

	return ts
}

func (ts *TestScenario) ValueFrom(supplier ValueSupplier) ReadyTestScenario {
	return ts.Value(supplier())
}

func (ts *TestScenario) ValueFromUnsafe(supplier UnsafeValueSupplier) ReadyTestScenario {
	newValue, err := supplier()
	if err != nil {
		ts.fail(fmt.Sprintf("[%s] failure occured: %v", ts.path, err))
	}

	ts.value = newValue

	return ts
}

func (ts *TestScenario) Map(mapping Function) ReadyTestScenario {
	return ts.ValueFrom(func() interface{} {
		return mapping(ts.value)
	})
}

func (ts *TestScenario) MapUnsafe(mapping UnsafeFunction) ReadyTestScenario {
	return ts.ValueFromUnsafe(func() (interface{}, error) {
		return mapping(ts.value)
	})
}

func (ts *TestScenario) Consume(consumer Consumer) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		consumer(ts.value)
		return ts, nil
	})
}

func (ts *TestScenario) ConsumeUnsafe(consumer UnsafeConsumer) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		return ts, consumer(ts.value)
	})
}

func (ts *TestScenario) Load(fileName string, loader file.Loader) ReadyTestScenario {
	return ts.ValueFromUnsafe(func() (interface{}, error) {
		return ts.onInputFile(fileName, func(inputFile *os.File) (interface{}, error) {
			return loader.Load(inputFile)
		})
	})
}

func (ts *TestScenario) Save(saver file.Saver, fileName string) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		_, err := ts.onActualFile(fileName, func(actualFile *os.File) (interface{}, error) {
			return nil, saver.Save(ts.value, actualFile)
		})

		return ts, err
	})
}

func (ts *TestScenario) Compare(fileName string, comparator file.Comparator) ReadyTestScenario {
	return ts.continueWithScenario(func() (ReadyTestScenario, error) {
		_, err := ts.onActualFile(fileName, func(actualFile *os.File) (interface{}, error) {
			_, err := ts.onExpectedFile(fileName, func(expectedFile *os.File) (interface{}, error) {
				if err := comparator.Compare(actualFile, expectedFile); err != nil {
					return nil, err
				}

				return nil, nil
			})

			return nil, err
		})

		if err != nil {
			ts.moveToErrorDirectory(fileName)
		}

		return ts, err
	})
}

func (ts *TestScenario) continueWithScenario(supplier ScenarioSupplier) ReadyTestScenario {
	newScenario, err := supplier()
	if err != nil {
		ts.fail(fmt.Sprintf("[%s] failure occured: %v", ts.path, err))
	}

	return newScenario
}

func (ts *TestScenario) onInputFile(fileName string, operation func(*os.File) (interface{}, error)) (interface{}, error) {
	return ts.onFile(filepath.Join(ts.inputDirectory, fileName), operation)
}

func (ts *TestScenario) onActualFile(fileName string, operation func(*os.File) (interface{}, error)) (interface{}, error) {
	return ts.onFile(filepath.Join(ts.actualDirectory, fileName), operation)
}

func (ts *TestScenario) onExpectedFile(fileName string, operation func(*os.File) (interface{}, error)) (interface{}, error) {
	return ts.onFile(filepath.Join(ts.expectedDirectory, fileName), operation)
}

func (ts *TestScenario) onErrorFile(fileName string, operation func(*os.File) (interface{}, error)) (interface{}, error) {
	return ts.onFile(filepath.Join(ts.errorDirectory, fileName), operation)
}

func (ts *TestScenario) onFile(filePath string, operation func(*os.File) (interface{}, error)) (interface{}, error) {
	f := ts.getOrCreateFile(filePath)
	defer f.Close()
	return operation(f)
}

func (ts *TestScenario) getOrCreateFile(filePath string) *os.File {
	ts.createDirectory(filePath)
	f, err := os.Open(filePath)
	if err != nil && os.IsNotExist(err) {
		nf, err := os.Create(filePath)
		if err != nil {
			ts.fail(fmt.Sprintf("could not create file %s : %v", filePath, err))
		}

		return nf
	}

	return f
}

func (ts *TestScenario) createDirectory(filePath string) {
	directory := filepath.Dir(filePath)
	err := os.MkdirAll(directory, os.ModePerm)
	if err != nil {
		ts.fail(fmt.Sprintf("could create directory %s : %v", directory, err))
	}
}

func (ts *TestScenario) moveToErrorDirectory(fileName string) {
	_, _ = ts.onActualFile(fileName, func(actualFile *os.File) (interface{}, error) {
		_, _ = ts.onErrorFile(fileName, func(errorFile *os.File) (interface{}, error) {
			if _, err := io.Copy(errorFile, actualFile); err != nil {
				ts.fail("could copy file to error folder")
			}

			return nil, nil
		})

		if err := os.Remove(actualFile.Name()); err != nil {
			ts.fail("could remove file from actual folder")
		}

		return nil, nil
	})
}

func (ts *TestScenario) fail(message string) {
	ts.t.Error(message)
	ts.t.FailNow()
}
