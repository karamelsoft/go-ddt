package json

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestCompare(t *testing.T) {

	tests := []file.CompareTest{
		{Name: "single map", Supplier: jsonComparator(), FileName: "person.json", WantErr: false},
		{Name: "array of maps", Supplier: jsonComparator(), FileName: "persons.json", WantErr: false},
		{Name: "non existing input file", Supplier: jsonComparator(), FileName: "doesnotexist.json", WantErr: true},
		{Name: "non existing output file", Supplier: jsonComparator(), FileName: "wrong.json", WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Compare(t, tt)
		})
	}
}

func jsonComparator() func() file.Comparator {
	return func() file.Comparator {
		return NewJsonComparator(Strict)
	}
}
