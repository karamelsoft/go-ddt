package json

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestSave(t *testing.T) {

	tests := []file.SaveTest{
		{Name: "single map", Supplier: jsonSaver(), Value: createExpectedMap(), FileName: "person.json"},
		{Name: "array of maps", Supplier: jsonSaver(), Value: createExpectedArrayOfMaps(), FileName: "persons.json"},
		{Name: "single struct", Supplier: jsonSaver(), Value: createExpectedStruct(), FileName: "person.json"},
		{Name: "array of structs", Supplier: jsonSaver(), Value: createExpectedArrayOfStructs(), FileName: "persons.json"},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Save(t, tt)
		})
	}
}

func jsonSaver() func() file.Saver {
	return NewJsonSaver
}
