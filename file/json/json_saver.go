package json

import (
	"encoding/json"
	"gitlab.com/karamelsoft/gotest/file"
	"io"
)

type saver struct{}

func NewJsonSaver() file.Saver {
	return &saver{}
}

func (s saver) Save(value interface{}, writer io.Writer) error {
	return json.NewEncoder(writer).Encode(value)
}
