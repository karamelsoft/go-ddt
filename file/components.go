package file

import (
	"io"
)

type Loader interface {
	Load(reader io.Reader) (interface{}, error)
}

type Saver interface {
	Save(value interface{}, writer io.Writer) error
}

type Comparator interface {
	Compare(actual, expected io.Reader) error
}
