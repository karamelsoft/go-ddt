package binary

import (
	"errors"
	"gitlab.com/karamelsoft/gotest/file"
	"io"
	"io/ioutil"
	"reflect"
)

type comparator struct{}

func NewBinaryComparator() file.Comparator {
	return &comparator{}
}

func (c comparator) Compare(actual, expected io.Reader) error {
	actualBinary, err := ioutil.ReadAll(actual)
	if err != nil {
		return err
	}

	expectedBinary, err := ioutil.ReadAll(expected)
	if err != nil {
		return err
	}

	if !reflect.DeepEqual(actualBinary, expectedBinary) {
		return errors.New("byte arrays are different")
	}

	return nil
}
