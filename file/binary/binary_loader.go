package binary

import (
	"gitlab.com/karamelsoft/gotest/file"
	"io"
	"io/ioutil"
)

type loader struct{}

func NewBinaryLoader() file.Loader {
	return &loader{}
}

func (loader loader) Load(reader io.Reader) (interface{}, error) {
	return ioutil.ReadAll(reader)
}
