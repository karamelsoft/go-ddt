package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestSave(t *testing.T) {

	tests := []file.SaveTest{
		{Name: "content", Supplier: textSaver(), Value: "Robert Patoulatchy", FileName: "new_person.txt"},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Save(t, tt)
		})
	}
}

func textSaver() func() file.Saver {
	return NewTextSaver
}
