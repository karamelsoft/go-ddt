package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"gitlab.com/karamelsoft/gotest/file/binary"
	"io"
)

type saver struct{}

func NewTextSaver() file.Saver {
	return &saver{}
}

func (s saver) Save(value interface{}, writer io.Writer) error {
	text := value.(string)
	return binary.NewBinarySaver().Save([]byte(text), writer)
}
