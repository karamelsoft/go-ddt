package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestLoad(t *testing.T) {

	tests := []file.LoadTest{
		{Name: "existing file", Supplier: textLoader(), FileName: "person.txt", Want: "Frederic Gendebien", WantErr: false},
		{Name: "non existing file", Supplier: textLoader(), FileName: "doesnotexist.txt", Want: nil, WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Load(t, tt)
		})
	}
}

func textLoader() func() file.Loader {
	return NewTextLoader
}
