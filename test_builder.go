package gotest

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

const (
	TestDirectory = ".tests"
)

type TestBuilder interface {
	Single(scenario Scenario)
	ForEach(scenario Scenario)
}

func NewTest(t *testing.T, name string) TestBuilder {
	return &test{
		t:    t,
		name: name,
		path: filepath.Join(TestDirectory, name),
	}
}

type test struct {
	t    *testing.T
	name string
	path string
}

func (test test) Single(scenario Scenario) {
	test.createTestDirectory()
	test.t.Run(test.name, func(t *testing.T) {
		NewScenario(t, test.path).Execute(scenario)
	})
}

func (test *test) ForEach(scenario Scenario) {
	test.createTestDirectory()

	_ = filepath.Walk(test.path, func(path string, info os.FileInfo, err error) error {
		if isScenarioDirectory(test.path, path, info) {
			scenarioName := filepath.Base(path)
			test.t.Run(scenarioName, func(t *testing.T) {
				NewScenario(t, path).Execute(scenario)
			})
		}

		return nil
	})
}

func (test *test) createTestDirectory() {
	_, err := os.Stat(test.path)
	if os.IsNotExist(err) {
		if err = os.MkdirAll(test.path, os.ModePerm); err != nil {
			log.Fatalf("could not create folders: %s : %v", test.path, err)
		}
	}
}

func isScenarioDirectory(testPath, path string, info os.FileInfo) bool {
	testPathLength := len(strings.Split(testPath, "/"))
	scenarioPathLength := len(strings.Split(path, "/"))

	return info != nil &&
		info.IsDir() &&
		scenarioPathLength == (testPathLength+1)
}
